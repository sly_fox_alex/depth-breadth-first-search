from graph import Graph

g = Graph()
random_graph = g.graph_tree()
############################ dfs
vesit = [False for i in range(len(random_graph))]
############################# bfs
level_g = [False for i in range(len(random_graph))];


def dfs(ves):
    vesit[ves] = True
    try:
        for sm_ver in random_graph[ves]:
            if not vesit[sm_ver]:
                print(sm_ver)
                dfs(sm_ver)
    except:
        print("fail")


def bfs(ver: object, level: object) -> object:
    level_g[ver] = level
    next_level = -1
    for next_ver in random_graph[ver]:
        if not level_g[next_ver]:
            next_level = level + 1;
            level_g[next_ver] = next_level;
    return next_level


if __name__ == "__main__":
    print("dfs>>")
    print("************************************")
    print(random_graph)
    print("Visit vertex from root:")
    dfs(0)
    print(vesit)

    print("search last vertexes: ")
    for ver_free in range(len(vesit)):
        if not vesit[ver_free]:
            print(ver_free)
            dfs(ver_free)

    print(vesit)
    print("************************************")
    print("bfs>> \n")
    level_g[0] = -1;
    this_level = bfs(0, 0)
    while (True):
        for vertex in range(len(random_graph)):
            if level_g[vertex] == this_level:
                bfs(vertex, this_level)

        this_level_n = max(level_g)
        if (this_level != this_level_n):
            this_level = this_level_n
        else:
            break

    print(level_g)
    print(random_graph)
