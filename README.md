# Breadth-first search  AND  Depth-first search
Depth-first search (DFS) is an algorithm for traversing or searching tree or graph data structures. The algorithm starts at the root node (selecting some arbitrary node as the root node in the case of a graph) and explores as far as possible along each branch before backtracking.
Breadth-first search (BFS) is an algorithm for traversing or searching tree or graph data structures. It starts at the tree root (or some arbitrary node of a graph, sometimes referred to as a 'search key'), and explores all of the neighbor nodes at the present depth prior to moving on to the nodes at the next depth level.
# How the algorithm works 
dfs>>
************************************
[[0, 1], [3, 2], [4], [4], [4, 5], [7, 6, 11], [6, 7], [9, 8, 10], [8, 10, 9], [9], [10, 11], [12, 14, 16], [12, 18, 13], [14], [14, 20, 15], [15, 17], [16], [18, 24, 23], [], [], [], [], [], [], [], [], []]
Visit vertex from root:
1
3
4
5
7
9
8
10
11
12
18
13
14
20
15
17
24
23
16
6
2
[True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, False, True, False, False, True, True, False, False]
search last vertexes: 
19
21
22
25
26
[True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True]
************************************
bfs>> 

[1, 1, 2, 2, 3, 4, 5, 5, 6, 6, 6, 5, 6, 7, 6, 7, 6, 8, 7, False, 7, False, False, 9, 9, False, False]
[[0, 1], [3, 2], [4], [4], [4, 5], [7, 6, 11], [6, 7], [9, 8, 10], [8, 10, 9], [9], [10, 11], [12, 14, 16], [12, 18, 13], [14], [14, 20, 15], [15, 17], [16], [18, 24, 23], [], [], [], [], [], [], [], [], []]

(Написан для анкеты в академию самсунг)
